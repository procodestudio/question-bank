/**
 * app.js
 *
 * Root module
 * imports all necessary app modules
 */

import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import ngMessages from 'angular-messages';
import 'ngstorage';
import sortable from 'sortablejs';
import lodash from 'lodash';
import sortablejs from 'sortablejs';

import config from './app.config';
import css from './app.styl';
import shared from './shared/shared';
import components from './components/components';
import { AppComponent } from './app.component';

const root = angular
  .module('app', [
    uiRouter,
    ngMessages,
    'ngStorage',
    shared,
    components
  ])
  .config(config)
  .component('bcApp', AppComponent);

export default root;