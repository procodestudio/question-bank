/**
 * common/components/header/header.component.js
 *
 * A common component for the header of the application
 */

import css from './header.styl';

export const HeaderComponent = {
  template: require('./header.html')
};