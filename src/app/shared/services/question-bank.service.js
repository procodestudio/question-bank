/**
 * shared/services/question-bank.service.js
 *
 * A common service to store and retrieve
 * question bank options defined by user
 */

class QuestionBank {
  constructor() {
    this.segmentObj = {};
    this.categoryObj = {};
    this.momentObj = {};
    this.chosenObj = [];
  }

  get segment() {
    return this.segmentObj;
  }

  set segment(data) {
    this.segmentObj = data;
  }

  get category() {
    return this.categoryObj;
  }

  set category(data) {
    this.categoryObj = data;
  }

  get moment() {
    return this.momentObj;
  }

  set moment(data) {
    this.momentObj = data;
  }

  get chosen() {
    return this.chosenObj;
  }

  set chosen(data) {
    this.chosenObj = _.reject(this.chosenObj, function(item) {
      return item.segment._id === data.segment._id &&
        item.category._id === data.category._id &&
        item.moment._id === data.moment._id;
    });
    this.chosenObj.push(data);
  }

  getChosenGroup(segment, category, moment) {
    return _.find(this.chosenObj, function(item) {
      return item.segment._id === segment._id &&
        item.category._id === category._id &&
        item.moment._id === moment._id;
    });
  }
}

QuestionBank.$inject = [];

export const QUESTION_BANK_NAME = 'QuestionBank';
export default QuestionBank;