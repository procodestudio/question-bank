/**
 * shared/services/data.service.js
 *
 * A common service to request API data
 */

class DataService {
  constructor($http, $q) {
    this.$http = $http;
    this.$q = $q;
  }

  /**
   * Get data from API
   *
   * @param {object} config
   * @returns {Promise}
   */
  getData(config) {
    let request;
    let data = !config.data ? {} : config.data;

    request = this.$http({
      cache: false,
      method: config.method,
      data: this.objToParam(data),
      url: config.url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      timeout: 10000
    });

    return request.then(this.handleSuccess.bind(this), this.handleError.bind(this));
  }

  /**
   * Handle success response from server
   */
  handleSuccess(response) {
    return response.data;
  }

  /**
   * Handle failed response from server
   */
  handleError(error) {
    return this.$q.reject('Something wrong happened: Error ' + (error.status || error));
  }

  /**
   * Normalize data object before send it to API
   */
  objToParam(obj) {
    return Object.keys(obj).map((k) => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');
  }
}

DataService.$inject = ['$http', '$q'];

export const DATA_SERVICE_NAME = 'DataService';
export default DataService;