/**
 * shared/directives/field-cntr.directive.js
 *
 * Directive to apply classes to form inputs and labels
 */

export const FieldCntrDirective = function () {
  return {
    restrict: 'A',
    link: function ($scope, $el, $attr) {
      const label = $el.find('label');
      const input = $el.find('input');

      input.on('focus', function() {
        label.addClass('isActive');
      });

      input.on('blur', function() {
        if (trim(input.val()) === '') {
          input.val('');
          label.removeClass('isActive');
        }
      });
    }
  };
};

/**
 * Trim string
 *
 * @param {string} str
 * @returns {string|XML}
 */
function trim(str) {
  return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}