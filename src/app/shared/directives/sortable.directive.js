/**
 * shared/directives/sortable.directive.js
 *
 * Directive to apply SortableJS to an element
 */

export const SortableDirective = function () {
  return {
    restrict: 'A',
    scope: {
      config: '=sortable'
    },
    link: function ($scope, $el, $attr) {
      Sortable.create($el[0], $scope.config);
    }
  };
};