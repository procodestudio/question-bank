/**
 * shared/shared.js
 *
 * Module that wraps shared application components and services
 */

import angular from 'angular';
import DataService from './services/data.service';
import QuestionBank from './services/question-bank.service';
import { HeaderComponent } from './components/header/header.component';
import { SortableDirective } from './directives/sortable.directive';
import { FieldCntrDirective } from './directives/field-cntr.directive';
import { AppConstants } from './constants/app.constants';

const shared = angular
  .module('app.shared', [])
  .component('bcHeader', HeaderComponent)
  .constant('AppConstants', AppConstants)
  .directive('sortable', SortableDirective)
  .directive('fieldCntr', FieldCntrDirective)
  .service('DataService', DataService)
  .service('QuestionBank', QuestionBank)
  .name;

export default shared;