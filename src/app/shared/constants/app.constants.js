/**
 * common/constants/app.constants.js
 *
 * Contain the application constants
 */

let mainRoot = (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1') ?
  'http://localhost:3000/api' :
  'https://app.binds.co/api';

export const AppConstants = {
  API_ROOT: mainRoot
};