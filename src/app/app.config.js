/**
 * app.config.js
 *
 * App config file
 *
 * @param {provider} $urlRouterProvider
 * @param {provider} $locationProvider
 * @param {provider} $qProvider
 * @param {provider} $compileProvider
 * @param {provider} $httpProvider
 */

function appRoutes(
  $urlRouterProvider,
  $locationProvider,
  $qProvider,
  $compileProvider,
  $httpProvider
) {
  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
  $qProvider.errorOnUnhandledRejections(false);
  $compileProvider.debugInfoEnabled(false);
  $httpProvider.useApplyAsync(true);
}

appRoutes.$inject = [
  '$urlRouterProvider',
  '$locationProvider',
  '$qProvider',
  '$compileProvider',
  '$httpProvider'
];

export default appRoutes;