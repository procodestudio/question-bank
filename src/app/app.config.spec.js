'use strict';

import root from './app';

describe('App Initialization', function() {
  let $locationProvider;
  let $urlRouterProvider;
  let $qProvider;

  beforeEach(() => {
    angular.mock.module('ui.router');
    angular.mock.module((_$locationProvider_, _$urlRouterProvider_, _$qProvider_) => {
      $locationProvider = _$locationProvider_;
      $urlRouterProvider = _$urlRouterProvider_;
      $qProvider = _$qProvider_;

      spyOn($locationProvider, 'html5Mode');
      spyOn($urlRouterProvider, 'otherwise');
      spyOn($qProvider, 'errorOnUnhandledRejections');
    });

    angular.mock.module('app');
    inject();
  });

  it('should set html5 mode', () => {
    expect($locationProvider.html5Mode).toHaveBeenCalledWith(true);
  });

  it('should set otherwise router', () => {
    expect($urlRouterProvider.otherwise).toHaveBeenCalledWith('/');
  });

  it('should set $q to not throw errors on rejections', () => {
    expect($qProvider.errorOnUnhandledRejections).toHaveBeenCalledWith(false);
  });
});