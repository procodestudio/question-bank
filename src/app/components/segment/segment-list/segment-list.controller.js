/**
 * components/segment/segment-list/segment-list.controller.js
 *
 * Controller for <segment-list> component
 */

class SegmentListController {
  constructor($state) {
    this.$state = $state;
  }

  /**
   * Open <category-list> component
   *
   * @param {object} segment
   */
  openCategory(segment) {
    this.$state.go('category', {segment: segment.slug});
  }
}

SegmentListController.$inject = ['$state'];

export const SEGMENT_LIST_CONTROLLER_NAME = 'SegmentListController';
export default SegmentListController;