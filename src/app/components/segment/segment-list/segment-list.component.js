/**
 * components/segment/segment-list/segment-list.component.js
 *
 * Stateful Component that defines a segment list
 */

import controller from './segment-list.controller';
import css from './segment-list.component.styl';

export const SegmentListComponent = {
  template: require('./segment-list.html'),
  bindings: {
    segments: '<'
  },
  controller
};