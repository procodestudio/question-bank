/**
 * components/segment/segment-item/segment-item.component.js
 *
 * Stateless Component that defines a segment item
 */

import controller from './segment-item.controller';
import css from './segment-item.component.styl';

export const SegmentItemComponent = {
  template: require('./segment-item.html'),
  bindings: {
    segment: '<data',
    onOpenCategory: '&'
  },
  controller
};