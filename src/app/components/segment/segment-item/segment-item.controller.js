/**
 * components/segment/segment-item/segment-item.controller.js
 *
 * Controller for <segment-item> component
 */

class SegmentItemController {
  constructor() {}

  /**
   * Returns segment item slug to parent component
   */
  onClick() {
    this.onOpenCategory({$segment: this.segment});
  }
}

SegmentItemController.$inject = [];

export const SEGMENT_ITEM_CONTROLLER_NAME = 'SegmentItemController';
export default SegmentItemController;