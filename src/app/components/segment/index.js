/**
 * components/segment/index.js
 *
 * Module that contains the segments components and services
 */

import angular from 'angular';
import router from './segment.route';
import { SegmentListComponent } from  './segment-list/segment-list.component';
import { SegmentItemComponent } from  './segment-item/segment-item.component';

const segment = angular
  .module('segment', [])
  .config(router)
  .component('segmentList', SegmentListComponent)
  .component('segmentItem', SegmentItemComponent)
  .name;

export default segment;