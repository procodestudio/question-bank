/**
 * components/segment/segment.route.js
 *
 * Segment router config
 */

segmentRouter.$inject = ['AppConstants', '$stateProvider'];

function segmentRouter(AppConstants, $stateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      component: 'segmentList',
      resolve: {
        segments: ['DataService', DataService => {
          return DataService
            .getData({
              method: 'GET',
              url: `${AppConstants.API_ROOT}/questionBankSegments`
            }).then(
              response => response,
              error => console.log(error)
            );
        }]
      }
    });
}

export default segmentRouter;