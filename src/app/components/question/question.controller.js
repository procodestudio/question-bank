/**
 * components/question/question.controller.js
 *
 * Controller for <question> component
 */

class QuestionController {
  constructor($scope, $state, QuestionBank) {
    this.$scope = $scope;
    this.$state = $state;
    this.QuestionBank = QuestionBank;
    this.chosen = [];
  }

  $onChanges() {
    const data = this.data;

    this.moment = {
      _id: data._id,
      slug: data.slug,
      title: data.title,
      description: data.description
    };

    ({
      segment: this.segment,
      category: this.category,
      questions: this.questions
    } = this.data);
  }

  $onInit() {
    const chosenQuestions = this.QuestionBank.getChosenGroup(
      this.segment,
      this.category,
      this.moment
    );

    if (chosenQuestions) {
      this.chosen = chosenQuestions.chosen;
    }
  }

  $onDestroy() {
    this.saveChosenObject();
  }

  /**
   * Send chosen items do Question Bank service
   */
  saveChosenObject() {
    const chosenUpdatedObj = {
      segment: this.segment,
      category: this.category,
      moment: this.moment,
      chosen: this.chosen,
    };

    this.QuestionBank.chosen = chosenUpdatedObj;
  }

  /**
   * Stores chosen items into array and
   * creates a new chosen item object
   *
   * @param {object} data
   */
  keepChosen(data) {
    if (_.find(this.chosen, {_id: data.idToAdd})) {
      console.log('Item já existe na sua lista');
    } else {
      this.createChosenItems(data.itemsOnBox);
    }
  }

  /**
   * Create a new chosen items object to be bound
   *
   * @param {array} items
   */
  createChosenItems(items) {
    const newChosen = [];
    const questions = this.questions;

    _.each(items, id => newChosen.push(_.find(questions, {_id: id})));

    this.chosen = newChosen;
    this.$scope.$apply();
  }

  /**
   * Get updated chosen items and call method to bind
   * new created items do scope
   *
   * @param {array} items
   */
  updateChosenItems(items) {
    this.createChosenItems(items);
  }

  /**
   * Remove chosen item from object
   *
   * @param {int} itemId
   */
  removeChosenItem(itemId) {
    if (_.find(this.chosen, {_id: itemId})) {
      this.chosen = _.reject(this.chosen, {_id: itemId});
    } else {
      console.log('Um erro ocorreu ao excluir o item de sua lista');
    }
  }

  /**
   * Workaround to handle clone element with SortableJs
   *
   * @param {object} questions
   */
  resetQuestions(questions) {
    this.questions = questions;
  }

  /**
   * Save chosen items to localStorage to be used later
   */
  saveChosenItems() {
    if (this.chosen.length) {
      this.saveChosenObject();

      this.QuestionBank.model = {
        segment: this.segment,
        category: this.category,
        moment: this.moment
      };

      this.$state.go('my-model', {
        segment: this.segment.slug,
        category: this.category.slug,
        moment: this.moment.slug,
        ids: _.map(this.chosen, '_id').join(',')
      });
    }
  }
}

QuestionController.$inject = ['$scope', '$state', 'QuestionBank'];

export const QUESTION_CONTROLLER_NAME = 'QuestionController';
export default QuestionController;