/**
 * components/question/question-chosen/question-chosen.component.js
 *
 * Stateless Component that defines a list of chosen questions
 */

import css from './question-chosen.styl';
import controller from './question-chosen.controller';

export const QuestionChosenComponent = {
  template: require('./question-chosen.html'),
  bindings: {
    questions: '<',
    moment: '<',
    onAdd: '&',
    onUpdate: '&',
    onDelete: '&'
  },
  controller
};