/**
 * components/question/question-chosen/question-chosen.controller.js
 *
 * Controller for <question-chosen> component
 */

class QuestionChosenController {
  constructor() {
    this.sortableConfig = {
      group: {
        name: 'questions',
        pull: false,
        put: true,
      },
      sort: true,
      animation: 150,
      ghostClass: 'question-chosen__item--ghost',
      onAdd: evt => this.onAddEvent(evt.item),
      onUpdate: evt => this.onUpdateEvent()
    };
  }

  /**
   * Get item id and return list of chosen items on box
   * to update chosen items object on parent
   *
   * @param {object} item
   */
  onAddEvent(item) {
    const questions = document.querySelectorAll('.question-chosen li');
    const idToAdd = item.getAttribute('data-id');
    const itemsOnBox = [];

    _.each(questions, function (question) {
      itemsOnBox.push(question.getAttribute('data-id'));
    }.bind(this));

    this.onAdd({
      data: {
        idToAdd,
        itemsOnBox
      }
    });

    item.remove();
  }

  /**
   * Send the list of chosen questions to parent
   * with the new sorting order
   */
  onUpdateEvent() {
    const questions = document.querySelectorAll('.question-chosen li');
    const itemsOnBox = [];

    _.each(questions, function(question) {
      itemsOnBox.push(question.getAttribute('data-id'));
    });

    this.onUpdate({items: itemsOnBox});
  }

  /**
   * Delete item from chosen object
   *
   * @param {int} itemId
   */
  onDeleteItem(itemId) {
    this.onDelete({item: itemId});
  }
}

QuestionChosenController.$inject = [];

export const QUESTION_CHOSEN_CONTROLLER_NAME = 'QuestionChosenController';
export default QuestionChosenController;