/**
 * components/question/question-list/question-list.controller.js
 *
 * Controller for <question-list> component
 */

class QuestionListController {
  constructor($timeout, $scope, QuestionBank) {
    this.$timeout = $timeout;
    this.$scope = $scope;
    this.cloned = null;
    this.QuestionBank = QuestionBank;
    this.sortableConfig = {
      group: {
        name: 'questions',
        pull: 'clone',
        put: false,
      },
      sort: false,
      animation: 150,
      ghostClass: 'question-list__item--ghost',
      filter: '.isChosen',
      onClone: evt => this.onCloneEvent(evt),
      onEnd: () => this.onEndEvent()
    };
  }

  /**
   * Apply class to item if it was select by user
   *
   * @param {string} questionId
   * @returns {boolean}
   */
  isChosen(questionId) {
    if (_.find(this.chosen, {'_id': questionId})) {
      return true;
    }
    return false;
  }

  /**
   * Get cloned element
   *
   * @param {event} evt
   */
  onCloneEvent(evt) {
    this.cloned = evt.clone;
  }

  /**
   * On end clone update question list
   */
  onEndEvent() {
    this.cloned.remove();
    const questions = angular.copy(this.questions);

    this.$timeout(function() {
      this.onUpdate({questions: questions});
    }.bind(this), 0);
  }
}

QuestionListController.$inject = ['$timeout', '$scope', 'QuestionBank'];

export const QUESTION_LIST_CONTROLLER_NAME = 'QuestionListController';
export default QuestionListController;