/**
 * components/question/question-list/question-list.component.js
 *
 * Stateful Component that defines a list of questions
 */

import controller from './question-list.controller';
import css from './question-list.styl';

export const QuestionListComponent = {
  template: require('./question-list.html'),
  bindings: {
    questions: '<',
    chosen: '<',
    onUpdate: '&'
  },
  controller
};