/**
 * components/question/question.component.js
 *
 * Stateful Component that defines the question wrapper
 */

import controller from './question.controller';
import css from './question.styl';

export const QuestionComponent = {
  template: require('./question.html'),
  bindings: {
    data: '<'
  },
  controller
};