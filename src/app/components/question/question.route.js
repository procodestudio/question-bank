/**
 * components/question/question.route.js
 *
 * Questions router config
 */

questionRouter.$inject = ['AppConstants', '$stateProvider'];

function questionRouter(AppConstants, $stateProvider) {
  $stateProvider
    .state('question', {
      url: '/:segment/:category/:moment',
      component: 'question',
      resolve: {
        data: ['$transition$', 'DataService', ($transition$, DataService) => {
          const segment = $transition$.params().segment;
          const category = $transition$.params().category;
          const moment = $transition$.params().moment;

          return DataService
            .getData({
              method: 'GET',
              url: `${AppConstants.API_ROOT}/questionBankSegments/${segment}/categories/${category}/moments/${moment}`
            }).then(
              response => response,
              error => console.log(error)
            );
        }]
      }
    });
}

export default questionRouter;