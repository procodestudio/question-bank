/**
 * components/question/question-footer/question-footer.component.js
 *
 * Component to wrapper saving actions
 */

import controller from './question-footer.controller';
import css from './question-footer.styl';

export const QuestionFooterComponent = {
  template: require('./question-footer.html'),
  bindings: {
    chosen: '<',
    onSave: '&'
  },
  controller
};