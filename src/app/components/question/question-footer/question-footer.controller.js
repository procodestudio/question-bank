/**
 * components/question/question-footer/question-footer.controller.js
 *
 * Controller for <question-footer> component
 */

class QuestionFooterController {
  constructor() {}

  /**
   * Fires onclick action
   */
  onClick() {
    this.onSave();
  }
}

QuestionFooterController.$inject = [];

export const QUESTION_FOOTER_CONTROLLER_NAME = 'QuestionFooterController';
export default QuestionFooterController;