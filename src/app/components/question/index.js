/**
 * components/questions/index.js
 *
 * Module that contains Questions components and services
 */

import angular from 'angular';
import router from './question.route';
import css from './question.styl';
import { QuestionComponent } from './question.component';
import { QuestionListComponent } from './question-list/question-list.component';
import { QuestionChosenComponent } from './question-chosen/question-chosen.component';
import { QuestionItemComponent } from './question-item/question-item.component';
import { QuestionFooterComponent } from './question-footer/question-footer.component';

const modelTool = angular
  .module('question', [])
  .config(router)
  .component('question', QuestionComponent)
  .component('questionList', QuestionListComponent)
  .component('questionChosen', QuestionChosenComponent)
  .component('questionItem', QuestionItemComponent)
  .component('questionFooter', QuestionFooterComponent)
  .name;

export default modelTool;
