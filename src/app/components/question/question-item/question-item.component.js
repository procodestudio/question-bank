import css from './question-item.styl';

export const QuestionItemComponent = {
  template: require('./question-item.html'),
  bindings: {
    question: '<data',
    onClone: '&'
  }
};