/**
 * components/category/index.js
 *
 * Module that contains categories components and services
 */

import angular from 'angular';
import router from './category.route';
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryItemComponent } from './category-item/category-item.component';

const category = angular
  .module('category', [])
  .config(router)
  .component('categoryList', CategoryListComponent)
  .component('categoryItem', CategoryItemComponent)
  .name;

export default category;