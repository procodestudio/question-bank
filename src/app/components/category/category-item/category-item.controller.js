/**
 * components/category/category-item/category-item.controller.js
 *
 * Controller for <category-item> component
 */

class CategoryItemController {
  constructor() {}

  /**
   * Returns category item slug to parent component
   */
  onClick() {
    this.onOpenMoment({$category: this.category});
  }
}

CategoryItemController.$inject = [];

export const CATEGORY_ITEM_CONTROLLER_NAME = 'CategoryItemController';
export default CategoryItemController;