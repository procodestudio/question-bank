/**
 * components/category/category-list/category-list.component.js
 *
 * Stateless Component that defines a category item
 */

import controller from './category-item.controller';
import css from './category-item.component.styl';

export const CategoryItemComponent = {
  template: require('./category-item.html'),
  bindings: {
    category: '<data',
    onOpenMoment: '&'
  },
  controller
};