/**
 * components/category/category-list/category-list.controller.js
 *
 * Controller for <category-list> component
 */

class CategoryListController {
  constructor($state, QuestionBank) {
    this.$state = $state;
    this.QuestionBank = QuestionBank;
  }

  $onChanges() {
    const data = this.data;

    this.categories = data.categories;
    this.segment = {
      _id: data._id,
      slug: data.slug,
      title: data.title,
      description: data.description
    };
  }

  /**
   * Open <moment-list> component
   *
   * @param {object} category
   */
  openMoment(category) {
    this.$state.go('moment', {
      segment: this.segment.slug,
      category: category.slug
    });
  }
}

CategoryListController.$inject = ['$state', 'QuestionBank'];

export const CATEGORY_LIST_CONTROLLER_NAME = 'CategoryListController';
export default CategoryListController;