/**
 * components/category/category-list/category-list.component.js
 *
 * Stateful Component that defines a list of categories
 */

import controller from './category-list.controller';
import css from './category-list.component.styl';

export const CategoryListComponent = {
  template: require('./category-list.html'),
  bindings: {
    data: '<'
  },
  controller
};