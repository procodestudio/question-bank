/**
 * components/category/category.route.js
 *
 * Category router config
 */

categoryRouter.$inject = ['AppConstants', '$stateProvider'];

function categoryRouter(AppConstants, $stateProvider) {
  $stateProvider
    .state('category', {
      url: '/:segment',
      component: 'categoryList',
      resolve: {
        data: ['$transition$', 'DataService', ($transition$, DataService) => {
          const segment = $transition$.params().segment;

          return DataService
            .getData({
              method: 'GET',
              url: `${AppConstants.API_ROOT}/questionBankSegments/${segment}`
            }).then(
              response => response,
              error => console.log(error)
            );
        }]
      }
    });
}

export default categoryRouter;