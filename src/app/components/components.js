/**
 * components/components.js
 *
 * Module that wraps all necessary components to build the application
 */

import angular from 'angular';
import segment from './segment';
import category from './category';
import moment from './moment';
import question from './question';
import myModel from './my-model';

const components = angular
  .module('app.components', [
    segment,
    category,
    moment,
    question,
    myModel
  ])
  .name;

export default components;