/**
 * components/moment/moment.route.js
 *
 * Moment router config
 */

momentRouter.$inject = ['AppConstants', '$stateProvider'];

function momentRouter(AppConstants, $stateProvider) {
  $stateProvider
    .state('moment', {
      url: '/:segment/:category',
      component: 'momentList',
      resolve: {
        data: ['$transition$', 'DataService', ($transition$, DataService) => {
          const segment = $transition$.params().segment;
          const category = $transition$.params().category;

          return DataService
            .getData({
              method: 'GET',
              url: `${AppConstants.API_ROOT}/questionBankSegments/${segment}/categories/${category}`
            }).then(
              response => response,
              error => console.log(error)
            );
        }]
      }
    });
}

export default momentRouter;