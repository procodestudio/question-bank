/**
 * components/moment/moment-item/moment-item.controller.js
 *
 * Controller for <moment-item> component
 */

class MomentItemController {
  /**
   * Returns moment item slug to parent component
   */
  onClick() {
    this.onOpenModelTool({$moment: this.moment});
  }
}

MomentItemController.$inject = [];

export const MOMENT_ITEM_CONTROLLER_NAME = 'MomentItemController';
export default MomentItemController;