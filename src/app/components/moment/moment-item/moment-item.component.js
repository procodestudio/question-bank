/**
 * components/moment/moment-list/moment-list.component.js
 *
 * Stateless Component that defines a moment item
 */

import controller from './moment-item.controller';
import css from './moment-item.component.styl';

export const MomentItemComponent = {
  template: require('./moment-item.html'),
  bindings: {
    moment: '<data',
    onOpenModelTool: '&'
  },
  controller
};
