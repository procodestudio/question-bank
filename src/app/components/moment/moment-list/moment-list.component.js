/**
 * components/moment/moment-list/moment-list.component.js
 *
 * Stateful Component that defines a list of moments
 */

import controller from './moment-list.controller';
import css from './moment-list.component.styl';

export const MomentListComponent = {
  template: require('./moment-list.html'),
  bindings: {
    data: '<'
  },
  controller
};