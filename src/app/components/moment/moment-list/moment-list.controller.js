/**
 * components/moment/moment-list/moment-list.controller.js
 *
 * Controller for <moment-list> component
 */

class MomentListController {
  constructor($state) {
    this.$state = $state;
  }

  $onChanges() {
    const data = this.data;

    this.category = {
      _id: data._id,
      slug: data.slug,
      title: data.title,
      description: data.description
    };

    ({
      segment: this.segment,
      moments: this.moments
    } = this.data);
  }

  /**
   * Open <model-tool> component
   *
   * @param {object} moment
   */
  openModelTool(moment) {
    this.$state.go('question', {
      segment: this.segment.slug,
      category: this.category.slug,
      moment: moment.slug
    });
  }
}

MomentListController.$inject = ['$state'];

export const MOMENT_LIST_CONTROLLER_NAME = 'MomentListController';
export default MomentListController;
