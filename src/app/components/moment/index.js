/**
 * components/moment/index.js
 *
 * Module that contains moment components and services
 */

import angular from 'angular';
import router from './moment.route';
import { MomentListComponent } from './moment-list/moment-list.component';
import { MomentItemComponent } from './moment-item/moment-item.component';

const moment = angular
  .module('moment', [])
  .config(router)
  .component('momentList', MomentListComponent)
  .component('momentItem', MomentItemComponent)
  .name;

export default moment;