/**
 * components/my-model/index.js
 *
 * Module that Shows all questions chosen by user
 */

import angular from 'angular';
import router from './my-model.route';
import { MyModelComponent } from './my-model.component';
import { MyModelListComponent } from './my-model-list/my-model-list.component';
import { MyModelItemComponent } from './my-model-item/my-model-item.component';
import { MyModelContactComponent } from './my-model-contact/my-model-contact.component';
import { MyModelShareDirective } from './my-model-share-button/my-model-share-button.directive';

const myModel = angular
  .module('myModel', [])
  .config(router)
  .component('myModel', MyModelComponent)
  .component('myModelList', MyModelListComponent)
  .component('myModelItem', MyModelItemComponent)
  .component('myModelContact', MyModelContactComponent)
  .directive('myModelShare', MyModelShareDirective)
  .name;

export default myModel;