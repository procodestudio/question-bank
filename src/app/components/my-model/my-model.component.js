/**
 * components/my-model/my-model.component.js
 *
 * Stateless component to show user's saved question model
 */

import controller from './my-model.controller';
import css from './my-model.styl';

export const MyModelComponent = {
    template: require('./my-model.html'),
    bindings: {
      data: '<',
      $transition$: '<'
    },
    controller
};