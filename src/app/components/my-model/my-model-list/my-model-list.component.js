/**
 * components/my-model/my-model-list/my-model-list.component.js
 *
 * My Model List component
 */

import controller from './my-model-list.controller';
import css from './my-model-list.styl';

export const MyModelListComponent = {
  template: require('./my-model-list.html'),
  bindings: {
    questions: '<',
    onEdit: '&'
  },
  controller
};