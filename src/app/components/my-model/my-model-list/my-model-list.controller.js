/**
 * components/my-model/my-model-list/my-model-list.controller.js
 *
 * Controller for <my-model-list> component
 */

class MyModelListController {
  constructor() {}

  editModel() {
    this.onEdit();
  }
}

MyModelListController.$inject = [];

export const MY_MODEL_LIST_CONTROLLER_NAME = 'MyModelListController';
export default MyModelListController;