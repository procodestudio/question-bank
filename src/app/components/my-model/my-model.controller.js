/**
 * components/my-model/my-model.controller.js
 *
 * Controller for <my-model> component
 */

import { Toast } from 'toaster-js';

class MyModelController {
  constructor($state, $http, QuestionBank) {
    this.$state = $state;
    this.$http = $http;
    this.QuestionBank = QuestionBank;
    this.formActive = true;
  }

  $onInit() {
    if (!this.data) {
      this.$state.go('home');
    } else {
      const data = this.data;

      this.moment = {
        _id: data._id,
        slug: data.slug,
        title: data.title,
        description: data.description
      };

      ({
        segment: this.segment,
        category: this.category,
        chosen: this.chosen,
        questions: this.questions
      } = this.data);
    }

    if (!this.chosen && this.questions) {
      let ids;
      ids = this.$transition$.params().ids.split(',');
      ids = _.map(ids, id => String(id));

      this.chosen = _(this.questions)
        .keyBy('_id')
        .at(ids)
        .value();
    }
  }

  /**
   * Save current model and redirects to my models' page
   */
  editMyModel() {
    const QB = this.QuestionBank;
    const chosenObj = {
      segment: this.segment,
      category: this.category,
      moment: this.moment,
      chosen: this.chosen,
    };

    QB.segment = this.segment;
    QB.category = this.category;
    QB.moment = this.moment;
    QB.chosen = chosenObj;

    this.$state.go('question', {
      segment: this.segment.slug,
      category: this.category.slug,
      moment: this.moment.slug
    });
  }

  /**
   * Send user data to Binds leads manager
   *
   * @param {object} data
   */
  sendContact(data) {
    const formConfig = {
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
    };

    this.formActive = false;

    this.$http
      .post(
        'https://app.binds.co/api/leads',
        JSON.stringify(data),
        formConfig
      )
      .then(
        response => {
          this.formActive = true;
          new Toast('Dados enviados com sucesso!', Toast.TYPE_DONE, Toast.TIME_NORMAL);
        },
        error => {
          this.formActive = true;
          new Toast('Houve um erro ao enviar seus dados', Toast.TYPE_ERROR, Toast.TIME_NORMAL);
        }
      );
  }
}

MyModelController.$inject = ['$state', '$http', 'QuestionBank'];

export const MY_MODEL_CONTROLLER_NAME = 'MyModelController';
export default MyModelController;