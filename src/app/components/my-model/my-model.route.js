/**
 * components/my-model/my-model.route.js
 *
 * My Model router config
 */

myModelRouter.$inject = ['AppConstants', '$stateProvider'];

function myModelRouter(AppConstants, $stateProvider) {
  $stateProvider
    .state('my-model', {
      url: '/meu-modelo/:segment/:category/:moment/:ids',
      component: 'myModel',
      resolve: {
        data: ['$transition$', '$localStorage', 'DataService', ($transition$, $localStorage, DataService) => {
          const segment = $transition$.params().segment;
          const category = $transition$.params().category;
          const moment = $transition$.params().moment;

          return DataService
            .getData({
              method: 'GET',
              url: `${AppConstants.API_ROOT}/questionBankSegments/${segment}/categories/${category}/moments/${moment}`
            }).then(
              response => response,
              error => console.log(error)
            );
        }]
      }
    });
}

export default myModelRouter;