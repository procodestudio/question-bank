/**
 * components/my-model/my-model-item/my-model-item.component.js
 *
 * Component for My Model item
 */

import css from './my-model-item.styl';

export const MyModelItemComponent = {
  template: require('./my-model-item.html'),
  bindings: {
    question: '<data'
  }
};