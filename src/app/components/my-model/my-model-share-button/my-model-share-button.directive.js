/**
 * components/my-model/my-model-share-button.directive.js
 *
 * Directive to share my model page
 */

import { Toast } from 'toaster-js';

export const MyModelShareDirective = function () {
  return {
    restrict: 'A',
    link: function ($scope, $el, $attr) {
      $el.on('click', function(e) {
        const textArea = document.createElement('textarea');

        textArea.style.position = 'fixed';
        textArea.value = window.location.href;
        document.body.appendChild(textArea);
        textArea.select();
        copyUrl();
        document.body.removeChild(textArea);
      });
    }
  };
};

/**
 * Copy my model URL
 */
function copyUrl() {
  try {
    document.execCommand('copy');
    new Toast('O link do seu modelo foi copiado para o seu clipboard', Toast.TYPE_DONE, Toast.TIME_NORMAL);
  } catch (err) {
    new Toast('Houve um erro ao tentar copiar o link', Toast.TYPE_ERROR, Toast.TIME_NORMAL);
  }
}