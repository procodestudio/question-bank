/**
 * components/my-model/my-model-contact/my-model-contact.component.js
 *
 * My Model Contact box component
 */

import controller from './my-model-contact.controller';
import css from './my-model-contact.styl';

export const MyModelContactComponent = {
  template: require('./my-model-contact.html'),
  bindings: {
    isActive: '<',
    onSend: '&'
  },
  controller
};