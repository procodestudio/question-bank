/**
 * components/my-model/my-model-contact/my-model-contact.controller.js
 *
 * Controller for <my-model-contact> component
 */

class MyModelContactController {
    constructor() {}

    /**
     * Send form data to parent component
     */
    sendForm() {
      const formData = {
        name: this.name,
        email: this.email,
        company: this.company,
        phone: this.phone
      };

      this.onSend({data: formData});
    }
}

MyModelContactController.$inject = [];

export const MY_MODEL_CONTACT_CONTROLLER_NAME = 'MyModelContactController';
export default MyModelContactController;