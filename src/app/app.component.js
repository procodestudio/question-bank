/**
 * app.component.js
 *
 * Root component that wraps the entire application
 *
 * @type {{template: string}}
 */

export const AppComponent = {
  template: `
    <bc-header></bc-header>
    <ui-view></ui-view>
  `
};