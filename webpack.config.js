const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const jeet = require('jeet');
const axis = require('axis');
const rupture = require('rupture');

const extractPlugin = new ExtractTextPlugin('main.css', {
  allChunks: true
});

module.exports = {
  devtool: 'source-map',
  entry: './src/app/app.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: '/node_modules',
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015']
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: ['html-loader']
      },
      {
        test: /\.(css|styl)$/,
        use: extractPlugin.extract({
          use: [
            'css-loader',
            'stylus-loader'
          ]
        })
      },
      {
        test: /\.(png|jpg)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/',
              publicPath: 'img/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    extractPlugin,
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      minify: {collapseWhiteSpace: true},
      hash: true,
      template: './src/index.html'
    }),
    new webpack.LoaderOptionsPlugin({
      test: /\.styl$/,
      stylus: {
        default: {
          use: [jeet(), axis(), rupture()],
        }
      },
    }),
    new webpack.ProvidePlugin({
      'Sortable': 'sortablejs',
      '_': 'lodash'
    }),
    new CleanWebpackPlugin(['public'])
  ]
};