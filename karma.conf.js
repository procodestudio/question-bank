const webpack = require('webpack');
const path = require('path');

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [{pattern: 'spec.bundle.js', watched: false}],
    exclude: [],
    preprocessors: {'spec.bundle.js': ['webpack', 'coverage', 'sourcemap']},
    webpack: {
      devtool: 'inline-source-map',
      entry: [
        './src/app/app.js'
      ],
      module: {
        rules: [{
          test: /\.js$/,
          exclude: '/node_modules',
          use: [{
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              presets: ['env'],
              plugins: [['istanbul', {
                exclude: [
                  '**/*.spec.js'
                ]
              }]]
            }
          }]
        }, {
          test: /\.html$/,
          use: ['html-loader']
        }, {
          test: /\.(jpe?g|png|gif|svg)$/i,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/'
            }
          }]
        }, {
          test: /\.(css|styl)$/,
          use: [
            'css-loader',
            'stylus-loader'
          ]
        }]
      }
    },
    webpackMiddleware: {
      stats: 'errors-only'
    },
    reporters: ['spec', 'coverage', 'coverage-istanbul'],
    coverageReporter: {
      reporters: [
        {type: 'text-summary'},
      ]
    },
    coverageIstanbulReporter: {
      reports: ['html'],
      dir: 'coverage'
    },
    client: {
      captureConsole: true
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_LOG,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  });
};
