const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3000;

const api = require('./router/api');
const publicPath = path.resolve(__dirname, 'public');

// Express config
app.use(express.static(publicPath));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Routes
app.use('/api/questionBankSegments', api);
app.use(express.static('public'));

// HTML5 Fallback
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../public/index.html'));
});

app.listen(PORT, () => console.log(`Server is running on http://localhost:${PORT}`));