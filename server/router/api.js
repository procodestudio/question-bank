const express = require('express');
const path = require('path');
const router = express.Router();

router.get('/',
  (req, res) => res.sendFile(path.resolve(__dirname, '../db', 'segments.json'))
);

router.get('/:segment',
  (req, res) => res.sendFile(path.resolve(__dirname, '../db', 'categories.json'))
);

router.get('/:segment/categories/:category',
  (req, res) => res.sendFile(path.resolve(__dirname, '../db', 'moments.json'))
);

router.get('/:segment/categories/:category/moments/:moment',
  (req, res) => res.sendFile(path.resolve(__dirname, '../db', 'questions.json'))
);

module.exports = router;